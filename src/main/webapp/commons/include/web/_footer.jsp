<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<footer>
    <div class="container">
        <div class="footer-widgets">
            <div class="f-widget f-widget-1">
                <div class="widget">
                    <h3>网站相关</h3>
                    <ul>
                        <li>
                            <a title="Rss 订阅" target="_blank" href="${ctxPath}/rss.xml">Rss 订阅</a>
                        </li>
                        <li>
                            <a title="Git地址" target="_blank" rel="nofollow" href="${options.git_url}">本站源码</a>
                        </li>
                        <li>
                            <a title="新浪微博" target="_blank" rel="nofollow" href="${options.wb_sina}">新浪微博</a>
                        </li>
                        <li>
                            <a title="腾讯微博" target="_blank" rel="nofollow" href="${options.wb_qq}">腾讯微博</a>
                        </li>
                    </ul>
                    QQ : <a target="_blank" rel="nofollow" href="http://wpa.qq.com/msgrd?v=3&amp;uin=596392912&amp;site=qq&amp;menu=yes">596392912</a>
                </div>
            </div>
            <div class="f-widget f-widget-2">
                <div class="widget">
                    <h3>热门标签</h3>
                    <div class="tagcloud">
                        <db:sqls var="x" sqlKey="hostTags" limit="14">
                            <a title="${x.tagName}" href="${ctxPath}/tags/${x.tagName}">
                                ${x.tagName}
                                <span style="color:tan;">&nbsp;${x.num}</span>
                            </a>
                        </db:sqls>
                    </div>
                </div>
            </div>
            <div class="f-widget last">
                <div class="widget">
                    <h3>官方微信</h3><img height="120" alt="DreamLu官方微信" width="120" src="${options.cdn_path}/static/images/weixin.jpg?v=v1.1.1">
                </div>
            </div>
        </div><!--.footer-widgets-->
    </div>
    <div class="copyrights">
        <div id="copyright-note" class="row">
            <span>© 2013&nbsp;<a title="Google+" target="_blank" rel="nofollow" href="${options.google}">${options.site_name}</a></span>
            <span><a title="备案号" target="_blank" rel="nofollow" href="http://www.miitbeian.gov.cn/">京ICP备${options.record_number}号</a></span>友情链接：
            <span>
                <db:sqls var="x" sqlKey="friendLinks" parameters="0;true">
                    <a title="${x.title}" target="_blank" href="${x.url}">${x.title}</a>
                </db:sqls>
            </span>
            <a style="float: right;" rel="nofollow license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/cn/">
                <img height="15" style="border-width:0;vertical-align: middle;" alt="知识共享许可协议" width="80" src="${options.cdn_path}/static/images/licenses.png">
            </a>
        </div>
    </div><!--end copyrights-->
</footer>