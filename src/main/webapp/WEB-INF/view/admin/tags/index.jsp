<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%-- 填充head --%>
<layout:override name="head">
    <style>
        .dataTables_length select { width: 80px;}
        .fg-toolbar { height: 30px;}
    </style>
</layout:override>

<%-- 填充content --%>
<layout:override name="content">
    <c:set var="tagsClasses" value="in"></c:set>
    <%@ include file="/commons/include/admin/_admin_navbar.jsp" %>
    <div class="content">
        <div class="header">
            <h1 class="page-title">网站设置</h1>
        </div>
        <ul class="breadcrumb">
            <li>
                <a href="/admin">首页</a><span class="divider">/</span>
            </li>
            <li class="active">标签管理</li>
        </ul>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="well">
                    <table id="otable" cellpadding="0" cellspacing="0" border="0" width="100%" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="60%">标签名</th>
                            <th width="10%">文章数量</th>
                            <th width="20%">操作</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <%@ include file="/commons/include/admin/_admin_footer.jsp" %>
        </div>
    </div>
</layout:override>

<%-- 填充script --%>
<layout:override name="script">
    <script id="manageTemp" type="text/html">
        <a href="/admin/tags/delete/{{=id}}" class="delete_show">
            <i class="icon-remove"></i>删除
        </a>
    </script>
    <script type="text/javascript" src="${options.cdn_path}/static/js/admin/tags_list.js"></script>
</layout:override>

<%@ include file="/commons/_layout_admin.jsp" %>