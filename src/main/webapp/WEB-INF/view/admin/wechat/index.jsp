<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%-- 填充head --%>
<layout:override name="head">
    <style>
        .dataTables_length select { width: 80px;}
        .fg-toolbar { height: 30px;}
    </style>
</layout:override>

<%-- 填充content --%>
<layout:override name="content">
    <c:set var="wechatClasses" value="in"></c:set>
    <%@ include file="/commons/include/admin/_admin_navbar.jsp" %>
    <div class="content">
        <div class="header">
            <h1 class="page-title">微信管理</h1>
        </div>
        <ul class="breadcrumb">
            <li>
                <a href="${ctxPath}/admin">首页</a>
                <span class="divider">/</span>
            </li>
            <li class="active">规则列表</li>
        </ul>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="btn-toolbar">
                    <a href="${ctxPath}/admin/wechat/add_edit" class="btn btn-primary"><i class="icon-plus"></i>添加</a>
                    <div class="btn-group"></div>
                </div>
                <div class="well">
                    <table id="otable" cellpadding="0" cellspacing="0" border="0" width="100%" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="5%">id</th>
                            <th width="10%">规则</th>
                            <th width="10%">父规则</th>
                            <th width="50%">回复</th>
                            <th width="10%">说明</th>
                            <th width="10%">操作</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <%@ include file="/commons/include/admin/_admin_footer.jsp" %>
        </div>
    </div>
</layout:override>

<%-- 填充script --%>
<layout:override name="script">
    <script id="manageTemp" type="text/html">
        <a href="${ctxPath}/admin/wechat/add_edit/{{=id}}" style="margin-right: 8px;"><i class="icon-pencil"></i>编辑</a>
        <a href="${ctxPath}/admin/wechat/delete/{{=id}}" class="delete_show"><i class="icon-remove"></i>删除</a>
    </script>
    <script type="text/javascript" src="${options.cdn_path}/static/js/admin/wechat_list.js"></script>
</layout:override>

<%@ include file="/commons/_layout_admin.jsp" %>