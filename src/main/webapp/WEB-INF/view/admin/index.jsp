<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>

<%-- 填充content --%>
<layout:override name="content">
    <c:set var="dashboardClasses" value="in"></c:set>
    <%@ include file="/commons/include/admin/_admin_navbar.jsp" %>
    <div class="content">
        <div class="header">
            <h1 class="page-title">博文热度</h1>
        </div>
        <ul class="breadcrumb">
            <li class="active">首页</li>
        </ul>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="well">
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
            <%@ include file="/commons/include/admin/_admin_footer.jsp" %>
        </div>
    </div>
</layout:override>

<%-- 填充script --%>
<layout:override name="script">
    <script src="${options.cdn_path}/static/js/highcharts/highcharts.js"></script>
    <script src="${options.cdn_path}/static/js/highcharts/modules/exporting.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#container').highcharts({
                chart: {
                    type: 'scatter',
                    zoomType: 'xy'
                },
                title: {
                    text: '博文热度图'
                },
                subtitle: {
                    text: "${options.site_url}",
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    title: {
                        enabled: true,
                        text: '博文id'
                    },
                    startOnTick: true,
                    endOnTick: true,
                    showLastLabel: true
                },
                yAxis: {
                    title: {
                        text: '阅读数 （次）'
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    verticalAlign: 'top',
                    x: 100,
                    y: 70,
                    floating: true,
                    backgroundColor: '#FFFFFF',
                    borderWidth: 1
                },
                plotOptions: {
                    scatter: {
                        marker: {
                            radius: 5,
                            states: {
                                hover: {
                                    enabled: true,
                                    lineColor: 'rgb(100,100,100)'
                                }
                            }
                        },
                        states: {
                            hover: {
                                marker: {
                                    enabled: false
                                }
                            }
                        },
                        tooltip: {
                            headerFormat: '<b>{series.name}</b><br>',
                            pointFormat: 'ID:{point.x}, {point.y} 次'
                        }
                    }
                },
                series: [{
                    name: '统计',
                    color: 'rgba(223, 83, 83, .5)',
                    data: $.parseJSON('${data}')
                }]
            });
        });
    </script>
</layout:override>

<%@ include file="/commons/_layout_admin.jsp" %>
