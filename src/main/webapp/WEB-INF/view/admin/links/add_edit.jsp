<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>

<%-- 填充content --%>
<layout:override name="content">
    <c:if test="${links.id eq null}">
        <c:set var="tab" value="修改博文"></c:set>
    </c:if>
    <c:if test="${links.id ne null}">
        <c:set var="tab" value="写博"></c:set>
    </c:if>
    <c:set var="optionsClasses" value="in"></c:set>
    <%@ include file="/commons/include/admin/_admin_navbar.jsp" %>
    <div class="content">
        <div class="header">
            <h1 class="page-title">${tab}</h1>
        </div>
        <ul class="breadcrumb">
            <li>
                <a href="/admin">首页</a><span class="divider">/</span>
            </li>
            <li class="active">${tab}</li>
        </ul>
        <div class="container-fluid">
            <div class="row-fluid">
                <form action="/admin/links/save_update" method="post">
                    <input type="hidden" name="links.id" value="${links.id}"/>
                    <div class="btn-toolbar">
                        <button type="submit" class="btn btn-primary">
                            <i class="icon-save"> </i>&nbsp;保存
                        </button><a href="/admin/links" class="btn">返回</a>
                        <div class="btn-group"></div>
                    </div>
                    <div class="well">
                        <div id="myTabContent" class="tab-content">
                            <label>网站</label>
                            <input type="text" name="links.title" value="${links.title}" required="required" class="input-xlarge"/>
                            <label>分类</label>
                            <select id="DropDownTimezone" name="links.type" class="input-xlarge">
                                <c:if test="${links.type eq 0}">
                                    <option value="0" selected="selected">友链</option>
                                    <option value="1">广告</option>
                                </c:if>
                                <c:if test="${links.type eq 1}">
                                    <option value="0">友链</option>
                                    <option value="1" selected="selected">广告</option>
                                </c:if>
                            </select>
                            <label>地址</label>
                            <input type="text" name="links.url" value="${links.url}" class="input-xxlarge"/>
                            <label>图标</label>
                            <input type="text" name="links.img" value="${links.img}" class="input-xxlarge"/>
                        </div>
                    </div>
                </form>
            </div>
            <%@ include file="/commons/include/admin/_admin_footer.jsp" %>
        </div>
    </div>
</layout:override>

<%-- 填充script --%>
<layout:override name="script">
    <script type="text/javascript">
        $(function(){
            $('form').submit(function(){
                var dialog = $.dialog();
                var title = $('input[name="links.title"]').val();
                var url   = $('input[name="links.url"]').val();
                var img   = $('input[name="links.img"]').val();
                if( title.length < 1 ){
                    dialog.content('网站名不能为空！').time(1000);
                    return false;
                }
                if( url.length < 1 ){
                    dialog.content('链接地址不能为空！').time(1000);
                    return false;
                }
                _post(this, dialog, function(data){
                    if(data.code === 0){
                        dialog.content('保存成功！').lock().time(1000);
                        setTimeout(function(){
                            location.href = '${ctxPath}/admin/links';
                        }, 1500);
                    }else{
                        dialog.content('服务器忙，请稍候！').time(2000);
                    }
                })
                return false;
            });
        });
    </script>
</layout:override>

<%@ include file="/commons/_layout_admin.jsp" %>