<%@ page language="java" contentType="text/xml;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<?xml version="1.0" encoding="utf-8" ?>
<rss version="2.0">
    <channel>
        <title>DreamLu博客，JFinal开源博客！</title>
        <link>http://www.dreamlu.net/</link>
        <language>zh-cn</language>
        <description>DramLu：java,nodejs开源博客</description>
        <db:sqls var="x" sqlKey="rssList" limit="20">
            <item>
                <title><![CDATA[${x.title}]]></title>
                <link>http://www.dreamlu.net/blog/${x.id}</link>
                <guid>http://www.dreamlu.net/blog/${x.id}</guid>
                <description><![CDATA[${x:filterRss(x.content)}]]></description>
                <author>${x.nick_name}</author>
                <pubDate>${x:format(x.update_time, "E, d MMM yyyy HH:mm:ss z")}</pubDate>
            </item>
        </db:sqls>
    </channel>
</rss>