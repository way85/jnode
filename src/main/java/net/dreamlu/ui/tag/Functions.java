package net.dreamlu.ui.tag;


import com.jfinal.kit.StrKit;
import net.dreamlu.common.WebUtils;
import net.dreamlu.model.BlogTag;
import net.dreamlu.model.User;
import net.dreamlu.utils.DateUtil;
import net.dreamlu.utils.HtmlFilter;
import net.dreamlu.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * beetl 自定义函数
 * @author L.cm
 * email: 596392912@qq.com
 * site:http://www.dreamlu.net
 * date 2015年7月4日下午6:05:07
 */
public class Functions {

	/**
	 * 继续encode URL (url,传参tomcat会自动解码)
	 * 要作为参数传递的话，需要再次encode
	 * @param url
	 * @return String
	 */
	public String encodeUrl(String url) {
		try {
			url = URLEncoder.encode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// ignore
		}
		return url;
	}

	/**
	 * 获取登录的用户
	 * @return
	 */
	public User currentUser(HttpServletRequest request, HttpServletResponse response) {
		return WebUtils.currentUser(request, response);
	}
	
	public static String format(Date date, String pattern) {
		return DateUtil.format(date, pattern);
	}
	
	// 清除字符串中的html标签
	public static String filterText(String string) {
		return HtmlFilter.getText(string);
	}

	// 清除rss字符串中的html标签
	public static String filterRss(String string) {
		string = HtmlFilter.getText(string);
		return string.replaceAll("\\s*|\t|\r|\n","").replaceAll("&nbsp;", "");
	}

	// 清除字符串中的html标签并截取
	public static String filterSubText(String string, Integer length) {
		//string = string.replaceAll("&nbsp;", "");
		return StringUtils.subCn(HtmlFilter.getText(string), length, "...");
	}
	
	// 页面description，seo
	public static String description(String string) {
		string = string.replaceAll("\\s*|\t|\r|\n","").replaceAll("&nbsp;", "");
		return StringUtils.subCn(HtmlFilter.getText(string), 90, "...");
	}
	
	// 页面keyWords，seo，分词
	public static String keyWords(Integer blogId) {
		List<BlogTag> list = BlogTag.dao.findByCache(blogId);
		Set<String> tags = new HashSet<String>();
		for (BlogTag blogTag : list) {
			tags.add(blogTag.getStr("tag_name"));
		}
		return StringUtils.join(tags, ',');
	}
	
	// 标记关键字
	public static String markKeywords(String string, Integer length, String keywords) {
		if (StrKit.notBlank(keywords)) {
			return HtmlFilter.markKeywods(keywords, filterSubText(string, length));
		} else {
			return filterSubText(string, length);
		}
	}
	
	// 标签帮助，根据博文查找该博文的标签集合
	public static String bTags(Integer blogId) {
		StringBuilder html = new StringBuilder();
		List<BlogTag> list = BlogTag.dao.findByCache(blogId);
		if(null != list && list.size() > 0) {
			html.append("<span class=\"mt_icon\"></span>");
		}
		String temp = "<a title=\"{}\" href=\"/tags/{}\">{}</a>";
		for (int i = 0; i < list.size(); i++) {
			BlogTag blogTag = list.get(i);
			html.append(temp.replace("{}", blogTag.getStr("tag_name")));
			if (i != list.size() - 1 ) {
				html.append(',');
			}
		}
		return html.toString();
	}

	/**
	 * 从博文中获取一张图片
	 * @param content
	 * @return url
	 */
	public static String getBlogImg(String content) {
		return HtmlFilter.getImgSrc(content);
	}
}
