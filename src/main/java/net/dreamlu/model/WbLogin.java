package net.dreamlu.model;

import net.dreamlu.model.base.BaseWbLogin;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class WbLogin extends BaseWbLogin<WbLogin> {
	public static final WbLogin dao = new WbLogin();

	// "id" -> 主键id
	// "openId" -> 第三方id
	// "userId" -> 用户id
	// "createTime" -> 创建时间
	// "nickName" -> 昵称
	// "headPhoto" -> 头像
	// "type" -> 类别
	// "status" -> 状态

	/**
	 * 根据openid查找
	 * @param openid
	 * @return
	 */
	public WbLogin findByOpenID(String openid, String type) {
		String sql = "SELECT wb.* FROM wb_login wb WHERE wb.open_id = ? AND wb.type = ? limit 1";
		return this.findFirst(sql, openid, type);
	}
}
