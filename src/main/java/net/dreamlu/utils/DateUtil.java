package net.dreamlu.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.time.DateUtils;


/**
 * 格式化 时间
 * @author L.cm
 * @date 2013-5-15 下午1:52:03
 */
public class DateUtil {
	
	/**
	 * 格式化时间
	 */
	public static String format(Date date, String pattern) {
		DateFormat df = new SimpleDateFormat(pattern, Locale.CHINA);
		return df.format(date);
	}
	
	/**
	 * hour小时之前
	 */
	public static Date hourBefor(int hour){
		return DateUtils.addHours(new Date(), -hour);
	}
}
