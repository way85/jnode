package net.dreamlu.ext.base;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Page;

import net.dreamlu.vo.DataTables;

public class BaseController extends Controller {
	
	protected final Log logger = Log.getLog(this.getClass());
	
	// index
	public void index(){}
	
	/**
	 * DataTable渲染
	 * @param page
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void renderDataTable(Page page) {
		int draw = getParaToInt("draw", 0);
		renderJson(new DataTables(draw, page));
	}
	
}
