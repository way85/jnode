package net.dreamlu.controller.web;

import com.jfinal.core.Controller;
import net.dreamlu.model.Blog;
import net.dreamlu.model.BlogTag;

import java.util.List;

/**
 * 博文详情
 * @author L.cm
 * @date 2013-5-14 下午5:08:12
 */
public class BlogController extends Controller {
	
	public void index() {
		int id = getParaToInt(0, 1);
		Blog blog = Blog.dao.findFallById(id);
		// 对于没有的博文跳转到404
		if (null == blog) { 
			renderError(404);
		}
		// 更新浏览量
		Blog.updateViewCountById(id);
		// 查找标签tags，标签相关
		List<BlogTag> taglist = BlogTag.dao.findByCache(id);
		setAttr("blog", blog);
		setAttr("tags", taglist);
		render("blog.jsp");
	}
	
}
