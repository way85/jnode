package net.dreamlu.controller.web;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import net.dreamlu.common.Consts;
import net.dreamlu.common.WebUtils;
import net.dreamlu.model.Blog;
import net.dreamlu.model.MailVerify;
import net.dreamlu.model.User;
import net.dreamlu.model.WbLogin;
import net.dreamlu.utils.DateUtil;
import net.dreamlu.validator.EmailValidator;
import net.dreamlu.vo.AjaxResult;

import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 首页
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2013-5-7 上午9:42:21
 */
public class IndexController extends Controller {

	/**
	 * 首页
	 * @return void	返回类型
	 * @throws
	 */
	public void index() {
		Map<String, Object> result = new HashMap<String, Object>();
		Page<Blog> page = Blog.dao.page(getParaToInt(0, 1), Consts.BLOG_PAGE_SIZE, result);
		setAttr("blogPage", page);
		render("index.jsp");
	}
	
	/**
	 * 文章
	 * @return void	返回类型
	 * @throws
	 */
	public void blogs() {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("blog_type", 0);
		Page<Blog> page = Blog.dao.page(getParaToInt(0, 1), Consts.BLOG_PAGE_SIZE, result);
		setAttr("blogPage", page);
		setAttr("postsby", "原创");
		setAttr("actionUrl", "/blogs/");
		render("index.jsp");
	}
	
	/**
	 * 收藏
	 * @return void	返回类型
	 * @throws
	 */
	public void favorites() {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("blog_type", 1);
		Page<Blog> page = Blog.dao.page(getParaToInt(0, 1), Consts.BLOG_PAGE_SIZE, result);
		setAttr("blogPage", page);
		setAttr("postsby", "收藏");
		setAttr("actionUrl", "/favorites/");
		render("index.jsp");
	}
	
	/**
	 * 搜索
	 * @return void	返回类型
	 * @throws
	 */
	public void search() {
		Map<String, Object> result = new HashMap<String, Object>();
		String s = getPara("s");
		if (StrKit.isBlank(s)) {
			s = getPara(0);
		}
		result.put("s", s);
		int pageNum = getParaToInt(1, 1);
		Page<Blog> page = Blog.dao.page(pageNum, Consts.BLOG_PAGE_SIZE, result);
		setAttr("keywords", s);
		setAttr("blogPage", page);
		setAttr("postsby", "搜索：" + s);
		setAttr("actionUrl", "/search/" + s + "-");
		render("index.jsp");
	}
	
	/**
	 * @throws Exception 
	 * 标签 url /tags/nginx-1
	 * @return void	返回类型
	 * @throws
	 */
	public void tags() throws Exception {
		String tags = getPara(0);
		if (StrKit.isBlank(tags)) {
			redirect("/");
			return;
		}
		tags = URLDecoder.decode(tags,"utf-8");
		int pageNum = getParaToInt(1, 1);
		Page<Blog> page = Blog.dao.pageByTags(pageNum, Consts.BLOG_PAGE_SIZE, tags);
		setAttr("blogPage", page);
		setAttr("postsby", "tags：" + tags);
		setAttr("actionUrl", "/tags/" + tags + "-");
		render("index.jsp");
	}
	
	/**
	 * 关于
	 * @return void	返回类型
	 * @throws
	 */
	public void about() {}
	
	/**
	 * 登录
	 * @return void	返回类型
	 * @throws
	 */
	public void sign_in() {
		User user = getSessionAttr("user");
		if(StrKit.notNull(user)) {
			redirect("/admin");
		}else {
			render("../admin/sign-in.jsp");
		}
	}
	
	/**
	 * 重置密码
	 * @return void	返回类型
	 * @throws
	 */
	@Before(EmailValidator.class)
	public void reset_pwd() {
		String mailTo = getPara("email");
		User user = User.dao.findByEmail(mailTo);
		boolean status = false;
		if (null != user) {
//			Map<String, Object> model = new HashMap<String, Object>();
//			Options options = Options.dao.findByCache();
//			String pwd = StringUtils.randomPwd(8);
//			user.set(User.PASSWORD, StringUtil.pwdEncrypt(pwd)).update();
//			model.put("options", options);
//			model.put("user", user.getStr(User.NICK_NAME));
//			model.put("pwd", pwd);
//			status = MailKit.sendTemplateMail("找回密码-DreamLu.net", mailTo, model, "reset_pwd.html");
		}
		renderJson(new AjaxResult(status));
	}

	/**
	 * 邮箱返回连接
	 */
	public void finish() {
		String code = getPara("code");
		MailVerify mv = MailVerify.dao.getByCode(code);
		Date createTime = mv.getCreateTime();
		if (null != mv && createTime.after(DateUtil.hourBefor(24))) {
			WbLogin wb = WbLogin.dao.findById(mv.getId());
			wb.setStatus(1);
			wb.update();
			User user = User.dao.findById(wb.getUserId());
			user.setEmailVerify(1);
			wb.update();

			WebUtils.loginUser(this, user, true);
			redirect("/admin");
		} else {
			redirect("/");
		}
	}
	
	/**
	 * 登出
	 * @return void	返回类型
	 * @throws
	 */
	public void logout() {
		WebUtils.logoutUser(this);
		redirect("/sign_in");
	}
}
