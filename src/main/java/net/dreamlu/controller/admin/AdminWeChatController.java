package net.dreamlu.controller.admin;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;

import net.dreamlu.ext.base.BaseController;
import net.dreamlu.interceptor.AdminInterceptor;
import net.dreamlu.model.WxLeaveMsg;
import net.dreamlu.model.WxRule;
import net.dreamlu.vo.AjaxResult;

/**
 * 微信管理
 * @author L.cm
 * @date Sep 22, 2013 8:45:54 PM
 */
@Before(AdminInterceptor.class)
public class AdminWeChatController extends BaseController {

	/**
	 * 后台 微信规则 列表
	 * @return void	返回类型
	 * @throws
	 */
	public void list_json() {
		int iDisplayStart = getParaToInt("iDisplayStart", 0);
		int pageSize = getParaToInt("iDisplayLength", 10);
		int pageNum =  iDisplayStart / pageSize + 1;

		String search = getPara("sSearch");
		renderJson(WxRule.dao.pageDataTables(pageNum, pageSize, search));
	}

	/**
	 * 后台添加或跟新规则
	 * @return void	返回类型
	 * @throws
	 */
	public void add_edit() {
		Integer id = getParaToInt();
		if(null != id){
			WxRule rule = WxRule.dao.findById(id);
			setAttr("wxRule", rule);
		}
	}

	/**
	 * 保存或更新规则
	 * @return void	返回类型
	 * @throws
	 */
	public void save_update() {
		WxRule rule = getModel(WxRule.class);
		boolean state = false;
		if(null == rule.getId()){
			state = rule.save();
		} else {
			state = rule.update();
		}
		renderJson(new AjaxResult(state));
	}

	/**
	 * 删除规则
	 * @return void	返回类型
	 * @throws
	 */
	public void delete() {
		WxRule rule = WxRule.dao.findById(getParaToInt());
		boolean temp = false;
		if(StrKit.notNull(rule)){
			temp = rule.delete();
		} 
		renderJson(new AjaxResult(temp));
	}
	
	/**
	 * 留言管理
	 * @return void	返回类型
	 * @throws
	 */
	public void leave_msgs() {}
	
	/**
	 * 留言列表
	 * @return void	返回类型
	 * @throws
	 */
	public void msgs_list_json() {
		int iDisplayStart = getParaToInt("iDisplayStart", 0);
		int pageSize = getParaToInt("iDisplayLength", 10);
		int pageNum =  iDisplayStart / pageSize + 1;
		String sEcho = getPara("sEcho", "1");
		String search = getPara("sSearch");
		renderDataTable(WxLeaveMsg.dao.pageDataTables(pageNum, pageSize, sEcho, search));
	}

	/**
	 * 删除留言
	 * @return void	返回类型
	 * @throws
	 */
	public void delete_msg() {
		WxLeaveMsg msg = WxLeaveMsg.dao.findById(getParaToInt());
		boolean temp = false;
		if(StrKit.notNull(msg)){
			temp = msg.delete();
		} 
		renderJson(new AjaxResult(temp));
	}
}
