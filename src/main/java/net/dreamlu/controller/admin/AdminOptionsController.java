package net.dreamlu.controller.admin;

import com.jfinal.aop.Before;
import net.dreamlu.ext.base.BaseController;
import net.dreamlu.interceptor.AdminInterceptor;
import net.dreamlu.model.Options;
import net.dreamlu.vo.AjaxResult;

/**
 * 网站设置
 * @author L.cm
 * @date 2013-6-4 上午10:16:14
 */
@Before(AdminInterceptor.class)
public class AdminOptionsController extends BaseController {
    
    /**
     * 跟新网站配置
     * @Title: save_update
     * @return void    返回类型
     * @throws
     */
    public void save_update(){
    	Options options = getModel(Options.class);
		boolean state = false;
		if(null == options.getId()){
			state = options.saveOptions();
		} else {
			state = options.updateOptions();
		}
		renderJson(new AjaxResult(state));
    }
}
