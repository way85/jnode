package net.dreamlu.controller.admin;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import net.dreamlu.common.WebUtils;
import net.dreamlu.ext.base.BaseController;
import net.dreamlu.interceptor.AdminInterceptor;
import net.dreamlu.model.Blog;
import net.dreamlu.model.BlogTag;
import net.dreamlu.model.User;
import net.dreamlu.vo.AjaxResult;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 后台博文管理
 * @author L.cm
 * @date 2013-5-31 下午9:47:55
 */
@Before(AdminInterceptor.class)
public class AdminBlogController extends BaseController {

	/**
	 * 后台 blog 列表
	 * @return void	返回类型
	 * @throws
	 */
	public void list_json() {
		Integer iDisplayStart = getParaToInt("iDisplayStart", 0);
		Integer pageSize = getParaToInt("iDisplayLength", 10);
		Integer pageNum =  iDisplayStart / pageSize + 1;
		String search = getPara("sSearch");

		renderDataTable(Blog.dao.pageDataTables(pageNum, pageSize, search));
	}

	/**
	 * 后台写博或跟新
	 * @return void	返回类型
	 * @throws
	 */
	public void add_edit() {
		Integer id = getParaToInt();
		if(null != id){
			Blog blog = Blog.dao.findById(id);
			List<BlogTag> list = BlogTag.dao.findByBlogId(blog.getId());
			setAttr("blog", blog);
			setAttr("tags", list);
		}
	}

	/**
	 * @throws IOException 
	 * 保存或更新博客
	 * @return void	返回类型
	 * @throws
	 */
	public void save_update() {
		Blog blog = getModel(Blog.class);
		// seo 添加图片alt信息
		String title = blog.getTitle();
		String content = blog.getContent();
		if (StrKit.notBlank(title, content)) {
			blog.setContent(content.replaceAll("alt=\"\"", "alt=\"" + title + "\""));
		}
		boolean state = false;
		User user = WebUtils.currentUser(this);
		if(null == blog.getId()){
			blog.setUserId(user.getId());
			state = blog.save();
		} else {
			state = blog.update();
		}
		//===========================================
		// 发送动弹，[title](url)
		// 从缓存中拿取accessToken，避免accessToken被刷新
//		String accessToken = OscLogin.cacheToken();
//		if (StrKit.notBlank(accessToken)) {
//			OauthOsc.me().tweetPub(accessToken, '[' + title + "](" + Consts.DOMAIN_URL + "/blog/" + blog.getInt(Blog.ID) + ')');
//		}
		//===========================================
		// 同步标签，写的不够好...
		Integer blogId = blog.getId();
		List<Integer> list = BlogTag.dao.findIdsByBlogId(blogId);
		Integer[] tags = getParaValuesToInt("tags");
		// 1.没有标签
		if (null == list && null == tags) {
			renderJson(new AjaxResult(state));
			return;
		}
		// 2.数据库没有该博文的标签
		if (null == list) {
			BlogTag.dao.saveAllTags(blogId, tags);
			renderJson(new AjaxResult(state));
			return;
		}
		// 3.删除完tags
		if (null == tags) {
			BlogTag.dao.removeAllTags(blogId);
			renderJson(new AjaxResult(state));
			return;
		}
		List<Integer> tagsList = Arrays.asList(tags);
		// 4.没有改变
		if (list.size() == tagsList.size() && list.containsAll(tagsList)) {
			renderJson(new AjaxResult(state));
			return;
		}
		// 5.交叉部分 因为标签不可能那么多所以整的比较简单
		// L.cm 2014-05-04 优化为《编写高质量代码：改善java程序的151个建议》 130条
		// 5.01去除已有的和空字符串
		tagsList.removeAll(list);
		tagsList.remove("");
		if (tagsList.size() > 0) {
			BlogTag.dao.saveAllTags(blogId, tagsList.toArray());
		}
		// 5.02删除数据库已经不存在的
		list.removeAll(tagsList);
		list.remove("");
		if (list.size() > 0) {
			BlogTag.dao.removeTags(blogId, list.toArray());
		}
		renderJson(new AjaxResult(state));
	}

	
	/**
	 * 删除或显示博文
	 * @return void	返回类型
	 * @throws
	 */
	public void delete_show() {
		Blog blog = Blog.dao.findById(getParaToInt());
		boolean temp = false;
		if(StrKit.notNull(blog)){
			int status = blog.getDelStatus();
			blog.setDelStatus(status == 1 ? 0 : 1);
			temp = blog.update();
		}
		renderJson(new AjaxResult(temp));
	}
}