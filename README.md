## jnode
Demo演示地址：[http://www.dreamlu.net](http://www.dreamlu.net)

如果有不明白或问题可联系email：596392912@qq.com Thanks！

## 文档
[【安装文档】](http://git.oschina.net/596392912/jnode/wikis/home)

## 鸣谢
1. [JFinal](http://www.oschina.net/p/jfinal)
2. [JFinal-ext](http://www.oschina.net/p/jfinal-ext)
3. [oschina](http://www.oschina.net/)
4. [portnine-free-bootstrap-theme](https://github.com/xiow/portnine-free-bootstrap-theme)
5. [wechat](http://git.oschina.net/gson/wechat)
6. [jetbrick-template](http://subchen.github.io/jetbrick-template/)
7. [artDialog](http://www.planeart.cn/?page_id=660)

## 成功案例
1. [分享一下](http://www.shareyx.com)
2. [fangshanjiuwang](http://fangshanjiuwang.com)

如果您正在使用jnode，欢迎告之。

## 交流群
如梦技术：[`237587118`](http://shang.qq.com/wpa/qunwpa?idkey=f78fcb750b4f72c92ff4d375d2884dd69b552301a1f2681af956bd32700eb2c0)

## 微信公众号
![dreamlu-weixin](http://www.dreamlu.net/images/weixin.jpg)

## 捐助共勉
<img src="http://soft.dreamlu.net/weixin-9.jpg" width = "200" alt="微信捐助" align=center />
<img src="http://soft.dreamlu.net/weixin-19.jpg" width = "200" alt="微信捐助" align=center />
<img src="http://soft.dreamlu.net/alipay.png" width = "200" alt="支付宝捐助" align=center />

<img src="http://soft.dreamlu.net/qq-9.jpg" width = "200" alt="QQ捐助" align=center />
<img src="http://soft.dreamlu.net/qq-19.jpg" width = "200" alt="QQ捐助" align=center />

## License

( The MIT License )