### blog
|列名|类型|长度|注释|
|:----| :---- | :----| :----|
|id|INT|10|主键id|
|title|VARCHAR|200|标题|
|titleEn|VARCHAR|200|英文标题|
|top|TINYINT|3|置顶|
|content|LONGTEXT|2147483647|博客内容|
|userId|INT|10|用户id|
|viewCount|INT|10|查看量|
|typeId|INT|10|博文类别|
|shareUrl|VARCHAR|128|来源地址|
|status|INT|10|删除状态|
|createTime|DATETIME|19|创建时间|
|updateTime|DATETIME|19|更新时间|

### blog_tag
|列名|类型|长度|注释|
|:----| :---- | :----| :----|
|id|INT|10|主键id|
|blogId|INT|10|博文id|
|tagId|INT|10|标签id|

### leave_msg
|列名|类型|长度|注释|
|:----| :---- | :----| :----|
|id|INT|10|主键id|
|email|VARCHAR|64|邮箱|
|message|TEXT|65535|内容|
|name|VARCHAR|64|用户名|

### links
|列名|类型|长度|注释|
|:----| :---- | :----| :----|
|id|INT|10|主键id|
|type|INT|10|类别|
|title|VARCHAR|100|标题|
|url|VARCHAR|256|链接地址|
|img|VARCHAR|256|图标|
|orders|INT|10|排序号|
|status|INT|10|状态|
|createTime|DATETIME|19|创建时间|
|updateTime|DATETIME|19|更新时间|

### mail_verify
|列名|类型|长度|注释|
|:----| :---- | :----| :----|
|id|INT|10|主键id|
|userId|INT|10|用户id|
|code|VARCHAR|50|校验码|
|createTime|DATETIME|19|创建时间|

### options
|列名|类型|长度|注释|
|:----| :---- | :----| :----|
|id|INT UNSIGNED|10|配置id|
|k|VARCHAR|64|配置名|
|v|TEXT|65535|配置值|
|autoload|TINYINT|3|是否自动加载 默认1：自动加载0：不自动加载|

### tags
|列名|类型|长度|注释|
|:----| :---- | :----| :----|
|id|INT|10|主键id|
|name|VARCHAR|100|标签名|

### user
|列名|类型|长度|注释|
|:----| :---- | :----| :----|
|id|INT|10|主键id|
|nickName|VARCHAR|50|昵称|
|password|VARCHAR|64|密码|
|email|VARCHAR|50|邮箱|
|emailVerify|INT|10|邮箱验证|
|headPhoto|VARCHAR|100|头像|
|sex|INT|10|性别|
|birthday|DATE|10|生日|
|status|INT|10|状态|
|lastLoginTime|DATETIME|19|最后登录时间|
|signature|VARCHAR|100|签名|
|url|VARCHAR|100|主页地址|
|liveness|INT|10|活跃度|
|contribution|INT|10|贡献度|
|authority|INT|10|基础权限|
|createTime|DATETIME|19|创建时间|
|updateTime|DATETIME|19|更新时间|

### wb_login
|列名|类型|长度|注释|
|:----| :---- | :----| :----|
|id|INT|10|主键id|
|openId|VARCHAR|64|第三方id|
|userId|INT|10|用户id|
|createTime|DATETIME|19|创建时间|
|nickName|VARCHAR|64|昵称|
|headPhoto|VARCHAR|128|头像|
|type|VARCHAR|64|类别|
|status|INT|10|状态|

### wx_leave_msg
|列名|类型|长度|注释|
|:----| :---- | :----| :----|
|id|INT|10|主键id|
|wxUser|VARCHAR|64|微信用户|
|msg|TEXT|65535|留言|

### wx_rule
|列名|类型|长度|注释|
|:----| :---- | :----| :----|
|id|INT|10|主键id|
|rule|VARCHAR|128|规则|
|pid|INT|10|父级id|
|reply|TEXT|65535|回复|
|directions|VARCHAR|200|匹配|

